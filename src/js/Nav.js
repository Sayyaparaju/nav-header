import React from "react";
import "./Nav.css";

export class Nav extends React.Component {
    render () {
        return (
            <ul className="nav nav-wrapper">
                <li className="nav-item">
                    <a className="nav-link active" href="#">Active</a>
                </li>
                <li className="nav-item dropdown">
                    <a className="nav-link dropdown-toggle"
                       href="#" role="button"
                       aria-haspopup="true"
                       aria-expanded="false"
                    >
                        Dropdown Multi
                    </a>
                    <div className="dropdown-menu dropdown-multi">
                        <div className="container">
                            <div className="row">
                                <div className="col">
                                    <div className="title">
                                        <span>Section header</span>
                                    </div>

                                    <a className="dropdown-item active" href="#">Action</a>
                                    <a className="dropdown-item" href="#">Another action</a>
                                    <a className="dropdown-item" href="#">Something else here</a>
                                </div>
                                <div className="col">
                                    <div className="title">
                                        <span>Section header</span>
                                    </div>

                                    <a className="dropdown-item" href="#">Action</a>
                                    <a className="dropdown-item" href="#">Another action</a>
                                    <a className="dropdown-item" href="#">Something else here</a>
                                </div>
                            </div>

                            <br/>

                            <div className="row">
                                <div className="col">
                                    <div className="title">
                                        <span>Section header</span>
                                    </div>

                                    <a className="dropdown-item" href="#">Action</a>
                                    <a className="dropdown-item" href="#">Another action</a>
                                    <a className="dropdown-item" href="#">Something else here</a>
                                </div>
                            </div>
                        </div>
                    </div>
                </li>

                <li className="nav-item dropdown">
                    <a className="nav-link dropdown-toggle" href="#" role="button"
                       aria-haspopup="true" aria-expanded="false">Dropdown</a>
                    <div className="dropdown-menu">
                        <a className="dropdown-item" href="#">Action</a>
                        <a className="dropdown-item" href="#">Another action</a>
                        <a className="dropdown-item" href="#">Something else here</a>
                    </div>
                </li>
            </ul>
        )
    }
}
